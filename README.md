# Bitbucket Pipelines Pipe: SonarQube Server scan

Scan your code with [SonarQube Server](https://www.sonarsource.com/products/sonarqube/) to detect Bugs, Vulnerabilities and Code Smells in up to 27 programming languages.

SonarQube Server is the leading product for Continuous Code Quality & Code Security. It supports most popular programming languages, including Java, JavaScript, TypeScript, C#, Python, C, C++, and many more. 

NOTE: For projects using Maven, Gradle, .NET, C, C++ or Objective-C, please consider executing a respective scanner directly instead of using this pipe:

- For Maven see [Scanner for Maven](https://redirect.sonarsource.com/doc/install-configure-scanner-maven.html).
- For Gradle see [Scanner for Gradle](https://redirect.sonarsource.com/doc/gradle.html).
- For a .NET solution see [Scanner for .NET](https://redirect.sonarsource.com/doc/install-configure-scanner-msbuild.html).
- For C and C++, this pipe supports only [AutoConfig mode](https://docs.sonarsource.com/sonarqube/latest/analyzing-source-code/languages/c-family/analysis-modes/). For Objective-C, C, and C++ Compilation Database mode see [Running the CFamily analysis](https://docs.sonarsource.com/sonarqube/latest/analyzing-source-code/languages/c-family/running-the-analysis/).

See [SonarQube Server docs](https://docs.sonarsource.com/sonarqube/latest/devops-platform-integration/bitbucket-integration/bitbucket-cloud-integration/) for more details on how to configure integration with Bitbucket Cloud.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: sonarsource/sonarqube-scan:<pipe version> # Ex: v4.0.0, See the latest version at https://bitbucket.org/bitbucket/product/features/pipelines/integrations?category=codequality&p=sonarsource/sonarqube-scan
  variables:
    SONAR_HOST_URL: ${SONAR_HOST_URL} # Get the value from the repository/workspace variable.
    SONAR_TOKEN: ${SONAR_TOKEN} # Get the value from the repository/workspace variable. You shouldn't set secret in clear text here.
    # EXTRA_ARGS: '<array of strings>'  # Optional
    # DEBUG: '<boolean>'  # Optional
```

## Variables

| Variable                | Usage                                                                                                                 |
|-------------------------|-----------------------------------------------------------------------------------------------------------------------|
| SONAR_HOST_URL (\*)     | The public url of your SonarQube Server instance. You should use a repository/workspace variable to store this value. |
| SONAR_TOKEN (\*)        | Your SonarQube Server access token. You should use a **secured** repository/workspace variable to store this value.   |
| EXTRA_ARGS              | Extra [analysis parameters](https://redirect.sonarsource.com/doc/analysis-parameters.html)                            |
| SONAR_SCANNER_OPTS      | Scanner JVM options for SonarQube Server < 10.6 (e.g. `-Xmx256m`)                                                     |
| SONAR_SCANNER_JAVA_OPTS | Scanner JVM options for SonarQube Server >= 10.6 (e.g. `-Xmx256m`)                                                    |
| DEBUG                   | Turn on extra debug information. Default to `false`.                                                                  |

_(\*) = required variable._

## Prerequisites

To run an analysis on your code, you will need the public url of your SonarQube Server instance, and an access token to it. These two values have to be provided to the pipe through `SONAR_HOST_URL` and `SONAR_TOKEN` variables. 

Project metadata, including the location to the sources to be analyzed, must be declared in the file `sonar-project.properties` in the base directory of your project:

```properties
sonar.projectKey=<replace with the key generated when setting up the project on SonarQube Server>

# relative paths to source directories. More details and properties are described
# in https://docs.sonarsource.com/sonarqube/latest/project-administration/analysis-scope/
sonar.sources=.
```

## Examples

Basic example:

```yaml
- step:
    script:
      - pipe: sonarsource/sonarqube-scan:<pipe version> # Ex: v4.0.0, See the latest version at https://bitbucket.org/bitbucket/product/features/pipelines/integrations?category=codequality&p=sonarsource/sonarqube-scan
        variables:
          SONAR_HOST_URL: ${SONAR_HOST_URL}
          SONAR_TOKEN: ${SONAR_TOKEN}
```

A bit more advanced example that reads the report produced by eslint, sets maximum memory to 512MB, and enables verbose output:

```yaml
- step:
    script:
      - pipe: sonarsource/sonarqube-scan:<pipe version> # Ex: v4.0.0, See the latest version at https://bitbucket.org/bitbucket/product/features/pipelines/integrations?category=codequality&p=sonarsource/sonarqube-scan
        variables:
          SONAR_HOST_URL: ${SONAR_HOST_URL}
          SONAR_TOKEN: ${SONAR_TOKEN}
          EXTRA_ARGS: ['-Dsonar.eslint.reportPaths=report.json']
          SONAR_SCANNER_OPTS: -Xmx512m # For SonarQube Server < 10.6
          SONAR_SCANNER_JAVA_OPTS: -Xmx512m # For SonarQube Server >= 10.6
          DEBUG: "true"
```

Example of full Bitbucket Pipe workflow that uses both `sonarsource/sonarqube-scan` and `sonarsource/sonarqube-quality-gate` pipes. The workflow builds, runs tests and checks the SonarQube Server Quality Gate before deploying a NodeJS application:

```yaml
image: node:10.15.3

clone:
  depth: full # SonarQube Server scanner needs the full history to assign issues properly

pipelines:
  default:
    - step:
        name: Build, run tests, analyze on SonarQube Server
        caches:
          - node
        script:
          - npm install
          - npm test
          - pipe: sonarsource/sonarqube-scan:<pipe version> # Ex: v4.0.0, See the latest version at https://bitbucket.org/bitbucket/product/features/pipelines/integrations?category=codequality&p=sonarsource/sonarqube-scan
            variables:
              SONAR_HOST_URL: ${SONAR_HOST_URL}
              SONAR_TOKEN: ${SONAR_TOKEN}
    - step:
        name: Check Quality Gate on SonarQube Server
        max-time: 5 # value you should use depends on the analysis time for your project
        script:
          - pipe: sonarsource/sonarqube-quality-gate:1.0.0
            variables:
              SONAR_TOKEN: ${SONAR_TOKEN}
    - step:
        name: Deploy to Production
        deployment: "Production"
        script:
          - echo "Good to deploy!"
```

## Support

To get help with this pipe, or to report issues or feature requests, please get in touch on [our community forum](https://community.sonarsource.com/tags/c/help/sq/bitbucketcloud).

If you are reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
