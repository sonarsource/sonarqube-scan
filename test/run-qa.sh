#!/bin/bash

# Helper functions for coloring output.
info() { echo -e "\\e[36m$*\\e[0m"; }
error() { echo -e "\\e[31m✗ $*\\e[0m"; }
success() { echo -e "\\e[32m✔ $*\\e[0m"; }

# Helper function to check if SonarQube Server is up and running.
check_sq_is_up() {
  local statusCall="$(curl --silent --user admin:admin http://127.0.0.1:9000/api/system/status)"
  local status="$(jq -r '.status' <<< "$statusCall")"
  if [[ ! $? -eq 0 ]]; then
    error "Failed to check if SonarQube Server is up and running."
    exit 1
  fi
  echo $status;
}

info "Build scanner action..."
docker build --no-cache -t sonarsource/sonarqube-scan:local .
if [[ ! $? -eq 0 ]]; then
  error "Failed to build the scanner action."
  exit 1
fi
success "Scanner action built."

info "Wait until SonarQube Server is up..."
sleep 10
isUp=$(check_sq_is_up)
until [[ "$isUp" == "UP" ]]; do
  sleep 1
  isUp=$(check_sq_is_up)
done
success "SonarQube Server is up and running."

info "Generate a new token..."
tokenCall=$(curl --silent --user admin:admin -d "name=token" http://127.0.0.1:9000/api/user_tokens/generate)
token="$(jq -r '.token' <<< "$tokenCall")"
if [[ -z "$token" ]]; then
  error "Failed to generate a new token."
  exit 1
fi
success "New token generated."

info "Test fail-fast if SONAR_TOKEN is omitted..."
docker run --workdir `pwd` --env SONAR_HOST_URL='http://host.docker.internal:9000' sonarsource/sonarqube-scan:local
if [[ $? -eq 0 ]]; then
  error "Should have failed fast."
  exit 1
fi
success "Correctly failed fast."

info "Test fail-fast if SONAR_HOST_URL is omitted..."
docker run --workdir `pwd` --env SONAR_TOKEN=$token sonarsource/sonarqube-scan:local
if [[ $? -eq 0 ]]; then
  error "Should have failed fast."
  exit 1
fi
success "Correctly failed fast."

info "Analyze project..."
mkdir storage_dir
export BITBUCKET_PIPE_STORAGE_DIR=`pwd`/storage_dir
cd test/example-project/
docker run -v `pwd`:/workdir -v $BITBUCKET_PIPE_STORAGE_DIR:$BITBUCKET_PIPE_STORAGE_DIR --workdir /workdir --add-host "host.docker.internal:$BITBUCKET_DOCKER_HOST_INTERNAL" --env SONAR_TOKEN=$token --env SONAR_HOST_URL='http://host.docker.internal:9000' --env=BITBUCKET_PIPE_STORAGE_DIR=$BITBUCKET_PIPE_STORAGE_DIR sonarsource/sonarqube-scan:local
if [[ ! $? -eq 0 ]]; then
  error "Couldn't run the analysis."
  exit 1
elif [[ ! -f "${BITBUCKET_PIPE_STORAGE_DIR}/report-task.txt" ]]; then
  error "Couldn't find the report task file. Analysis failed."
  exit 1
fi
success "Analysis successful."

echo "" # new line
echo "============================"
echo "" # new line
success "QA successful!"
