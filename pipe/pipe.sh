#!/bin/bash

set -eo pipefail

source "$(dirname "$0")/common.sh"

parse_environment_variables() {
  SONAR_HOST_URL=${SONAR_HOST_URL:?'SONAR_HOST_URL variable is missing.'}
  SONAR_TOKEN=${SONAR_TOKEN:?'SONAR_TOKEN variable is missing.'}
  EXTRA_ARGS=${EXTRA_ARGS:=""}
  SONAR_SCANNER_OPTS=${SONAR_SCANNER_OPTS:=""}
  DEBUG=${DEBUG:="false"}
}

parse_environment_variables

if [ ! -z ${EXTRA_ARGS_COUNT} ]; then
  init_array_var 'EXTRA_ARGS';
else
  IFS=' ' read -r -a EXTRA_ARGS <<< "${EXTRA_ARGS}"
fi

ALL_ARGS=("${EXTRA_ARGS[@]}")

if [[ "${DEBUG}" == "true" ]]; then
  ALL_ARGS+=("-X")
  debug "EXTRA_ARGS: ${EXTRA_ARGS}"
  debug "ALL_ARGS:${ALL_ARGS}"
  debug "SONAR_SCANNER_OPTS: ${SONAR_SCANNER_OPTS}"
fi

sonar-scanner "${ALL_ARGS[@]}"

cp .scannerwork/report-task.txt ${BITBUCKET_PIPE_STORAGE_DIR}/