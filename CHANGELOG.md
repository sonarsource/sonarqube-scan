# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 4.0.0

- major: Update Scanner CLI to 7.0.2

## 3.1.0

- minor: SONAR-23648 Rebrand repositories owned by the Analysis Experience squad

## 3.0.2

- patch: Change EXTRA_ARGS to support array syntax
- patch: Use root user in the Docker image

## 3.0.1

- patch: Update base image

## 3.0.0

- major: Update base Docker image and SonarScanner to 6.2.0

## 2.0.1

- patch: Update SonarScanner to 5.0.1

## 2.0.0

- major: Update SonarScanner to 5.0

## 1.2.0

- minor: Update SonarScanner to 4.8

## 1.1.0

- minor: Update SonarScanner to 4.7

## 1.0.0

- major: Initial version
